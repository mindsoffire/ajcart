
const express = require('express');
const cors = require('cors');
const bodyParser = require('body-parser');
const app = express();
app.use(cors());
//app.use(bodyParser.json()); // for parsing application/json
/* userCarts = [
    { userName: 'AndrewTan', contents: ['applle', 'orange', 'mango'], savedTime: '18Jan2018:greatsaleTime' },
    { userName: 'JamesPraise', contents: [], savedTime: 'new' },
    { userName: 'JohnGood', contents: [], savedTime: 'new' },
    { userName: 'LimBeng', contents: [], savedTime: 'new' },
    { userName: 'AhTee', contents: [], savedTime: 'new' },
    { userName: 'MongooseChimp', contents: [], savedTime: 'new' },
] */
userCarts = [];
var found = false;
toRegister = { userName: 'Please sign up first.', contents: [], savedTime: 'You have not signed up!' };
// npm run main.js 6000 ; nodemon main.js 5000 ; node main.js 4000
app.set('port', process.argv[2] || process.env.PORT || 3000);
var server = app.listen(app.get('port'), () => {
    console.log('Express server listening at http://(' + server.address().family + ')127.0.0.1:' + server.address().address + server.address().port);
});
app.get('/api/cart', (req, res) => {
    var toJSON = null;
    var fill = { userName: '', contents: [], savedTime: '' };
    found = false;
    console.log(req.query.uN);
    console.log(req.query.signU);
    for (let i of userCarts) {
        if (req.query.uN == i.userName) {
            toJSON = i;
            found = true;
        }
    }
    if (req.query.signU == 'false') {
        (!found) ? toJSON = toRegister : toJSON;
    }
    else if (req.query.signU == 'true') {
        if (!found) {
            fill.userName = req.query.uN;
            fill.savedTime = 'Thank you for signing up!';
            userCarts.push(fill);
            toJSON = fill;
        }
        else {
            fill.userName = req.query.uN;
            fill.savedTime = 'You have registered already!';
            toJSON = fill;
        }
    }
    //(toJSON)? toJSON : toJSON = toRegister;
    res.status(200).json(toJSON);
});
app.post('/api/cart', bodyParser.json(), (req, res) => {
    var toJSON = null;
    found = false;
    console.log(req.body);
    for (let i of userCarts) {
        if (req.body.userName == i.userName) {
            i.contents = req.body.contents;
            i.savedTime = new Date();
            console.log(i);
            toJSON = i;
            found = true;
        }
    }
    (!found)? toJSON = toRegister : toJSON;
    res.status(200).json(toJSON);
});
app.use(express.static(__dirname + '/public'));
app.use((req, res, next) => {
    res.redirect('/error.html');
});
