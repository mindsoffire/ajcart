import { Component } from '@angular/core';
import { FormControl, Validators, NgForm } from '@angular/forms';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import 'rxjs/add/operator/take';
import 'rxjs/add/operator/toPromise';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Shopping Cart';
  toRegister = false;

  cart = {
    userName: '',
    contents: [],
    savedTime: '',
  }
  uNPlaceHolder = 'Enter your username';

  //Inject HttpClient service into component
  constructor(private httpClient: HttpClient) { }


  loadOrSignUp(type: string) {

    this.toRegister = false;
    if (type == 'load') {
      let qs = new HttpParams()
        .set('uN', this.cart.userName)
        .set('signU', 'false');
      this.toRegister = true;
    }
    else if (type == 'signup') {
      let qs = new HttpParams()
        .set('uN', this.cart.userName)
        .set('signU', 'true');
      this.toRegister = false;
    }
    console.log(type);
    console.log(qs);
    this.httpClient.get(
      'https://ajcart.localtunnel.me/api/cart', { params: qs }
    )
      .take(1).toPromise()
      .then(result => {
        console.log('>> result = ', result);
        console.log('>> userName = ', result.userName);
        this.cart.savedTime = result.savedTime;
        if (result.savedTime == 'You have not signed up!') {
          this.toRegister = true;
          this.uNPlaceHolder = 'Please sign up first!';
          this.cart.userName = '';
        }
        else if (result.savedTime == 'Thank you for signing up!') {
          this.toRegister = false; this.cart.contents = result.contents;
        }
        else if (result.savedTime == 'You have registered already!')
          this.toRegister = false;
        else this.cart.contents = result.contents;
      })
      .catch(error => {
        console.error('>> error = ', error);
      });
  }


  onSubmitSave(f: NgForm) {
    console.log(this.cart);
    console.log(JSON.stringify(this.cart));
    this.httpClient.post(
      'https://ajcart.localtunnel.me/api/cart', this.cart
    )
      .take(1).toPromise()
      .then(result => {
        console.log('>> result = ', result);
        console.log('>> userName = ', result.userName);
        this.cart.savedTime = result.savedTime;
        if (result.savedTime == 'You have not signed up!') {
          this.toRegister = true;
          this.uNPlaceHolder = 'Please sign up first!';
          this.cart.userName = '';
        }
        else if (result.savedTime == 'Thank you for signing up!') {
          this.toRegister = false; this.cart.contents = result.contents;
        }
        else if (result.savedTime == 'You have registered already!')
          this.toRegister = false;
      })
      .catch(error => {
        console.error('>> error = ', error);
      });
  }

}
